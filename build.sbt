lazy val baseName = "ScreenStitch"

lazy val gitHost  = "codeberg.org"
lazy val gitUser  = "sciss"
lazy val gitRepo  = baseName

lazy val commonSettings = Seq(
  name          := baseName,
  version       := "1.0.0",
  organization  := "de.sciss",
  scalaVersion  := "2.13.8",
  description   := "Arrange various screenshots (typically from maps) and glue the parts together",
  homepage      := Some(url(s"https://$gitHost/$gitUser/$gitRepo")),
  licenses      := Seq("AGPL v3+" -> url("http://www.gnu.org/licenses/agpl-3.0.txt")),
  libraryDependencies += {
    "com.itextpdf" % "itextpdf" % "5.5.13.3"
  },
  scalacOptions ++= Seq("-deprecation", "-unchecked", "-feature", "-Xsource:2.13", "-Xlint")
)

lazy val root = project.in(file("."))
  .settings(commonSettings)
  .settings(publishSettings)

lazy val publishSettings = Seq(
  publishMavenStyle := true,
  publishTo := {
    Some(if (isSnapshot.value)
      "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"
    else
      "Sonatype Releases" at "https://oss.sonatype.org/service/local/staging/deploy/maven2"
    )
  },
  Test / publishArtifact := false,
  pomIncludeRepository := { _ => false },
  pomExtra := {
<scm>
  <url>git@{gitHost}:{gitUser}/{gitRepo}.git</url>
  <connection>scm:git:git@{gitHost}:{gitUser}/{gitRepo}.git</connection>
</scm>
<developers>
   <developer>
      <id>sciss</id>
      <name>Hanns Holger Rutz</name>
      <url>http://www.sciss.de</url>
   </developer>
</developers>
}
)
